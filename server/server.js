'use strict';

const express = require('express');
const http = require('http')

const socket = require('socket.io');
const SocketServer = require('./socket');
class Server{
    constructor()
    {
        this.port = 5000;
        this.host = 'localhost';

        this.app = express();
        this.http = http.Server(this.app); //Node Js Server
        this.socket = socket(this.http); //Run Socket io Module
    }
    runServer(){
        // This Socket Class

        new SocketServer(this.socket).socketConnection();
        //listen  a Node Js server

        this.http.listen(this.port,this.host,()=> {
            console.log(`The Server is Running at http://${this.host}:${this.port}`);
        });
    }
}
const app = new Server();
app.runServer();





















