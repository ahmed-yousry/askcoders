<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivatechatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privatechats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('massage');
            $table->integer('send_id')->unsigned();
            $table->foreign('send_id')->references('id')->on('users');
            $table->integer('receive_id')->unsigned();
            $table->foreign('receive_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privatechats');
    }
}
