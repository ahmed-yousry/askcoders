<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicchatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicchats', function (Blueprint $table) {
              $table->increments('id');
            $table->string('message');
            $table->string('from');
            $table->integer('from_uid')->unsigned();
            $table->foreign('from_uid')->references('id')->on('users');
            
            $table->timestamps();
     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicchats');
    }
}
