<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imagepublic extends Model
{
  protected $table = 'imagepublics';

   protected $fillable = [
        'user_id', 'image'
    ];
}
