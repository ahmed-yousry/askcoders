<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link  rel="stylesheet" href="{{ url('style_chat/css/bootstrap.min.css')}}">    
    <link rel="stylesheet"  href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    


       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js.map"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>


 <script src="{{ asset('js/privetetest.js') }}" defer></script>
 
    <link  rel="stylesheet" href="{{ url('style_chat/css/animate.css')}}"> 
     
    <link  rel="stylesheet" href=" {{ url('style_chat/css/style.css')}} "> 



  </head>
    <body>
        <section class="main">
            <div class="container-fluid">
                <div class="row">   
             


             @include('bar')
                                 
                    <div class="col-7 col-sm-7">         
                        <div class="main-right">
                            <h2>Community</h2> 
                            <div class="main-top">
                                <form>
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="search">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-search"></i></div>
                                        </div>
                                     </div>                                   
                                </form>                               
                                <a href="group-chat.html"><i class="fa fa-users"></i>create group chat</a>
                            </div>  
                            <div class="clear"></div>                                               
                            <div class="content">
                                <div class="row">
      @foreach(App\User::where('id','!=',auth()->user()->id)->get() as $user)
      <a href="{{url('home/privetmessage')}}/{{ $user->id }}"> 
                                            <div class="col-md-6">     
                                                <div class="person">
                                                    <div class="image">
                                                        <img src="{{url('uplode/'.$user->photopath)}}"alt="dashboared">
                                                    </div> 
                                                    <div class="text">
                                                        <div class="top">
                                                            <h4>{{ $user->name }}</h4>
                                                            <span>{{ $user->age }}</span></a>
                                                            <span class="circle"></span>
                                                        </div>
                                                        <div class="disc">
                                                            <p>{{ $user->description }}</p>
                                                        </div>
                                                        <div class="language">
                                                            <div class="native-lang">
                                                                <p>native</p>
                                                                <img src="{{ url('style_chat/images/Egypt-Flag.jpg')}}alt="dashboared">
                                                            </div>
                                                            <div class="nonnative-lang">
                                                                <p>learning</p>
                                                                <img src="{{ url('style_chat/images/non-native.png')}}"alt="dashboared">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clear"></div>
                                            </div>
                                      
                                 @endforeach         

                               
                                      
                                </div>  
                                <div class="chat-area">
                                    <!-- <div class="chat-box">
                                        <div class="chat-header">
                                            <p class="left">aaaa</p>
                                            <p class="right">
                                                <span class="minmize">-</span>
                                                <span class="close-chat">x</span>
                                            </p>                                        
                                        </div>
                                    </div> -->
                                </div>

                                <!-- <div class="tab-pane fade" id="non-native" role="tabpanel"aria-labelledby="nav-profile-tab">                          
                                        <div class="row">
                                            <div class="col-md-6">
                                                        <div class="person">
                                                                <div class="image">
                                                                    <img src="images/person.jpeg"alt="dashboared">
                                                                </div>
                                                                <div class="text">
                                                                    <div class="top">
                                                                        <h4>name</h4>
                                                                        <span>22</span>
                                                                        <span class="circle"></span>
                                                                    </div>
                                                                    <div class="disc">
                                                                        <p>Curso engenharia na UFMG, sou formado em técnico em automação industrial. Canto, toco violão, e gost</p>
                                                                    </div>
                                                                    <div class="language">
                                                                        <div class="native-lang">
                                                                            <p>native</p>
                                                                            <img src="images/Egypt-Flag.jpg"alt="dashboared">
                                                                        </div>
                                                                        <div class="nonnative-lang">
                                                                            <p>learning</p>
                                                                            <img src="images/non-native.png"alt="dashboared">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="clear"></div>
                                            </div>
                                            <div class="col-md-6">
                                                    <div class="person">
                                                            <div class="image">
                                                                <img src="images/person.jpeg"alt="dashboared">
                                                            </div>
                                                            <div class="text">
                                                                <div class="top">
                                                                    <h4>name</h4>
                                                                    <span>22</span>
                                                                    <span class="circle"></span>
                                                                </div>
                                                                <div class="disc">
                                                                    <p>Curso engenharia na UFMG, sou formado em técnico em automação industrial. Canto, toco violão, e gost</p>
                                                                </div>
                                                                <div class="language">
                                                                    <div class="native-lang">
                                                                        <p>native</p>
                                                                        <img src="images/Egypt-Flag.jpg"alt="dashboared">
                                                                    </div>
                                                                    <div class="nonnative-lang">
                                                                        <p>learning</p>
                                                                        <img src="images/non-native.png"alt="dashboared">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="clear"></div>
                                        </div>
                                        </div> 
                                </div> -->
                            </div>
                        </div>     
                    </div>   
                </div>
            </div>
        </section>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- <script>
            $(document).ready(function(){
               // $(".chat-box").hide();
                $("span.minmize").click(function (){          
                    alert("ee");
                    var color = $('.chat-box').css('bottom')         
                    if( color == '0') {
                        alert("ys");
                    }
                });           
                $(".close-chat").click(function(){
                    $(this).parentsUntil(".chat-box").remove();
                })     
                $(".chat-box").css("bottom","0");
                    $(".chat-box form").show();

                $(".main-right .person").click(function(){
                    var chat =   $(".chat-box").html();              
                    alert(chat);
                    // $(".chat-area").append('<div class="chat-box">' + chat + '</div>');                   
                    $(".chat-area").append('<div class="chat-box"><div class="chat-header"><p class="left">aaaa</p><p class="right"> <span class="minmize">-</span> <span class="close-chat">x</span></p></div>');                   
                });
                
            })
        </script> -->
    </body>
    </html>
