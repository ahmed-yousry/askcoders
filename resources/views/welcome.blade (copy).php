<html lang="ar">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link  rel="stylesheet" href="{{ url('style_chat/css/bootstrap.min.css')}}">    
    <link rel="stylesheet"  href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link  rel="stylesheet" href="{{ url('style_chat/css/animate.css')}}">    
    <link  rel="stylesheet" href="{{ url('style_chat/css/style.css')}}"> 



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js.map"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script src="{{ asset('js/test.js') }}" defer></script>
         
           <script type="text/javascript">
        var userid ='{{ auth()->user()->id }}';
        var userName ='{{auth()->user()->name}}';
    </script>



  </head>
    <body>
        <section class="main">
         <div class="container-fluid">
             <div class="row">              
                <div class="main-left">
                        <div class="text-center">
                            <div class="prof-img">
                                @foreach(App\User::where('id','=',auth()->user()->id)->get() as $user)
                                <img src="{{ url('uplode/'.$user->photopath)}}"alt="admin">
                                <a href="">{{$user->name}}</a>
                               
                            <!--    <a href="">{{auth()->user()->name}}</a>-->
                                  @endforeach
                            </div>                        
                            <ul class="list-unstyled">                            
                                            <li><a href="{{url('home')}}"><i class="fas fa-users"></i>rooms</a></li>    
                                            <li><a href="{{url('community')}}"><i class="fas fa-users"></i>community</a></li>    
                                            <li><a href="{{url('messages')}}"><i class="far fa-comments"></i>messages</a></li>  
                            </ul>
                        </div>
                </div>               
                <div class="main-right">
                        <h2>Community</h2>
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#native" role="tab" aria-controls="nav-home" aria-selected="true">native</a>
                                <a class="nav-item nav-link"        id="nav-profile-tab" data-toggle="tab" href="#non-native" role="tab" aria-controls="nav-profile" aria-selected="false">non native</a>                             
                            </div>
                        </nav>
                        <div class="buttons">
                            <div class="dropdown">
                                <button type="button"class="btn btn-primary dropdown-toggle"data-toggle="dropdown"><i class="fas fa-sliders-h"></i>filters</button>
                                <div class="dropdown-menu">
                                    <p class="dropdown-item">I WANT TO PRACTICE</p>
                                    <div class="lang">
                                        <img src="{{ url('style_chat/images/non-native.png')}}"alt="dashboared">
                                        <span>english</span>
                                        <span class="check"><i class="fa fa-check"></i></span>
                                    </div>
                                    <div class="lang">
                                        <img src="{{ url('style_chat/images/non-native.png')}}"alt="dashboared">
                                        <span>english</span>
                                        <span class="check"><i class="fa fa-check"></i></span>
                                    </div>
                                    <div class="lang">
                                        <img src="{{ url('style_chat/images/non-native.png')}}"alt="dashboared">
                                        <span>english</span>
                                        <span class="check"><i class="fa fa-check"></i></span>
                                    </div>  
                                    <p class="grey">Want to learn more languages?</p>  
                                    <a class="dropdown-item edit" href="#">Edit your profile</a>
                                    <form>
                                        <div class="dropdown-item">
                                            <label>gender</label>   
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" value="">Option 1
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" value="">Option 2
                                                </label>
                                            </div>
                                        </div>
                                        <div class="dropdown-item">
                                            <div class="form-group">
                                                <label for="formControlRange">age</label>
                                                <input type="range" class="form-control-range" id="formControlRange">
                                            </div>
                                        </div>
                                    <div class="dropdown-item">
                                        <select class="custom-select">
                                            <option selected>Open this select menu</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    <div class="right-buttons">
                                        <button type="button"class="btn-default">cancel</button>
                                        <button type="button"class="btn-primary">save</button>
                                    </div>
                                </form>                                 
                                </div>
                            </div>
                            <button type="button"class="btn btn-default">english</button>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="native" role="tabpanel"aria-labelledby="nav-home-tab">
                                <div class="row">


                               
                                @foreach(App\User::where('id','!=',auth()->user()->id)->get() as $user)
                                    <div class="col-md-6">
                                        <div class="person">
                                            <div class="image">
                                                <img src="{{url('uplode/'.$user->photopath)}}"alt="dashboared">
                                            </div>
                                            <div class="text" id="test" uid="{{$user->id}}" >
                                                <div class="top">
                                                    <h4>{{ $user->name }} </h4>
                                                    <span>{{ $user->age }}</span>
                                                    <span id="{{$user->id}}"></span>
                                                </div>
                                                <div class="disc">
                                                    <p>{{ $user->description }}</p>
                                                </div>
                                                <div class="language">
                                                    <div class="native-lang">
                                                        <p>native</p>
                                                        <img src="{{ url('style_chat/images/Egypt-Flag.jpg')}}"alt="dashboared">
                                                    </div>
                                                    <div class="nonnative-lang">
                                                        <p>learning</p>
                                                        <img src="{{ url('style_chat/images/non-native.png')}}"alt="dashboared">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
 @endforeach


                             
                  
                                </div>                                
                            </div>
                            <div class="tab-pane fade" id="non-native" role="tabpanel"aria-labelledby="nav-profile-tab">                          
                                <div class="row">
                                    <div class="col-md-6">
                                                <div class="person">
                                                        <div class="image">
                                                            <img src="{{ url('style_chat/images/person.jpeg')}}"alt="dashboared">
                                                        </div>
                                                        <div class="text">
                                                            <div class="top">
                                                                <h4>name</h4>
                                                                <span>22</span>
                                                                <span class="circle"></span>
                                                            </div>
                                                            <div class="disc">
                                                                <p>Curso engenharia na UFMG, sou formado em técnico em automação industrial. Canto, toco violão, e gost</p>
                                                            </div>
                                                            <div class="language">
                                                                <div class="native-lang">
                                                                    <p>native</p>
                                                                    <img src="{{ url('style_chat/images/Egypt-Flag.jpg')}}"alt="dashboared">
                                                                </div>
                                                                <div class="nonnative-lang">
                                                                    <p>learning</p>
                                                                    <img src="{{ url('style_chat/images/non-native.png')}}"alt="dashboared">
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="clear"></div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="person">
                                                    <div class="image">
                                                        <img src="{{ url('style_chat/images/person.jpeg')}}"alt="dashboared">
                                                    </div>
                                                    <div class="text">
                                                        <div class="top">
                                                            <h4>name</h4>
                                                            <span>22</span>
                                                            <span class="circle"></span>
                                                        </div>
                                                        <div class="disc">
                                                            <p>Curso engenharia na UFMG, sou formado em técnico em automação industrial. Canto, toco violão, e gost</p>
                                                        </div>
                                                        <div class="language">
                                                            <div class="native-lang">
                                                                <p>native</p>
                                                                <img src="{{ url('style_chat/images/Egypt-Flag.jpg')}}"alt="dashboared">
                                                            </div>
                                                            <div class="nonnative-lang">
                                                                <p>learning</p>
                                                                <img src="{{ url('style_chat/images/non-native.png')}}"alt="dashboared">
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="clear"></div>
                                </div>
                                </div> 
                        </div>
                    </div>
                </div>
          
         </div>
        </section>
        <script src="{{ url('style_chat/js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{ url('style_chat/js/popper.min.js')}}"></script>
        <script src="{{ url('style_chat/js/bootstrap.min.js')}}"></script>
    </body>
    </html>

