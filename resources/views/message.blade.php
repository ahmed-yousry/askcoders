<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link  rel="stylesheet" href="{{ url('style_chat/css/bootstrap.min.css')}}">    
    <link rel="stylesheet"  href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link  rel="stylesheet" href="{{ url('style_chat/lib/css/emoji.css')}}">    
    <link  rel="stylesheet" href="{{ url('style_chat/css/animate.css')}}"> 
     
    <link  rel="stylesheet" href=" {{ url('style_chat/css/style.css')}} ">    
  </head>
  <body>
    <section class="chat">
        <div class="container-fluid">
        <div class="row">  
  


  @include('bar')



            
            <div class="col-9 col-sm-9 no-padding">
                <div class="content">                    
                    <div class="top">
                        <h2 class="title">Inbox</h2>                             
                    </div>
                    <div class="clear"></div>
                    <div class="inbox">
                        <div class="single-chat">
                            <div class="online-friend">
                                <div class="image">
                                    <span class="circle"></span>
                                    <img src="images/person.jpeg" alt="admin">
                                    <!-- <span class="name">username</span> -->
                                </div>                                
                                <div class="username">
                                    <h2>username</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, placeat laborum aut amet ipsa iusto, quo doloribus incidunt odio itaque ullam, eligendi voluptatem quidem facilis porro enim dolore consequuntur vero?<span class="seen"><i class="fa fa-check"></i></span></p>
                                    <span></span>
                                </div>
                            </div>
                            <div class="online-friend">
                                <div class="image">
                                    <span class="circle"></span>
                                    <img src="images/person.jpeg" alt="admin">
                                    <!-- <span class="name">username</span> -->
                                </div>                                
                                <div class="username">
                                    <h2>username</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, placeat laborum aut amet ipsa iusto, quo doloribus incidunt odio itaque ullam, eligendi voluptatem quidem facilis porro enim dolore consequuntur vero? <span class="unseen"><i class="fa fa-check"></i></span> </p>
                                </div>
                            </div>
                            <div class="online-friend">
                                <div class="image">
                                    <span class="circle"></span>
                                    <img src="images/person.jpeg" alt="admin">
                                    <!-- <span class="name">username</span> -->
                                </div>                                
                                <div class="username">
                                    <h2>username</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, placeat laborum aut amet ipsa iusto, quo doloribus incidunt odio itaque ullam, eligendi voluptatem quidem facilis porro enim dolore consequuntur vero?</p>
                                </div>
                            </div>
                        </div>                      
                   </div>                    
                </div>                    
            </div>      
        </div> 
    </div> 
    </section>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="lib/js/config.js"></script>
    <script src="lib/js/util.js"></script>
    <script src="lib/js/jquery.emojiarea.js"></script>
    <script src="lib/js/emoji-picker.js"></script>
    <script>
            $(function() {
              // Initializes and creates emoji set from sprite sheet
              window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: '../lib/img/',
                popupButtonClasses: 'fa fa-smile-o'
              });
              // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
              // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
              // It can be called as many times as necessary; previously converted input fields will not be converted again
              window.emojiPicker.discover();
            });
    </script>
  </body>
  </html>