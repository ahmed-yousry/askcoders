<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link  rel="stylesheet" href="{{ url('style_chat/css/bootstrap.min.css')}}">    

    <link rel="stylesheet"  href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


    <link  rel="stylesheet" href="{{ url('style_chat/lib/css/emoji.css')}}">    
    <link  rel="stylesheet" href="{{ url('style_chat/css/animate.css')}}"> 
     
    <link  rel="stylesheet" href=" {{ url('style_chat/css/style.css')}} ">    
  </head>
  <body>
    <section class="chat">
        <div class="row">  
            
         



  @include('bar')






                    
            <div class="col-7 col-sm-7">
                <div>
                    <div>
                        <div class="top">
                            <h2 class="title">Account settings</h2>         
                       </div>
                       <div class="settings">
                          <form>
                            <div class="form-group row">
                              <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                              <div class="col-sm-7">
                                <input type="text"  class="form-control" id="staticEmail">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                              <div class="col-sm-7">
                                <input type="text"  class="form-control" id="staticEmail">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                              <div class="col-sm-7">
                                <input type="password" class="form-control" id="inputPassword" >
                                <a href="change-pasword.html"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              </div>
                            </div>
                            <div class="form-group row">                                
                                <label for="exampleFormControlTextarea1"class="col-sm-2 col-form-label">Biography</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">save changes</button>
                            </div>
                          </form>                
                        </div>
                    </div>                    
                </div>
            </div>    
        </div>
    </section>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="lib/js/config.js"></script>
    <script src="lib/js/util.js"></script>
    <script src="lib/js/jquery.emojiarea.js"></script>
    <script src="lib/js/emoji-picker.js"></script>
    <script>
            $(function() {
              // Initializes and creates emoji set from sprite sheet
              window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: '../lib/img/',
                popupButtonClasses: 'fa fa-smile-o'
              });
              // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
              // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
              // It can be called as many times as necessary; previously converted input fields will not be converted again
              window.emojiPicker.discover();
            });
    </script>
  </body>
  </html>